#define _USE_MATH_DEFINES

#include <iostream>
#include <complex>
#include <cmath>
#include <functional>
#include <fstream>

#define A 9
#define B 1
#define C 3
#define D 4
#define E 4
#define F 1

using namespace std;

struct DFT {
	double real = 0;
	double imag = 0;
};

void ton_prosty(double N, double a, double f, double przes, double *out, double dt)
{
	double t = 0;
	for (int i = 0; i < N; i++)
	{
		out[i] = a * sin((2. * M_PI * f * ((double)t)) + przes);
		t += dt;
	}
}

complex<double> wN(const double N)
{
	const double x = (2. * M_PI) / N;
	return complex<double>(cos(x), sin(x));
}



void dft(int N, double *val, complex<double> *out)
{
	/*const complex<double> wn = wN((double)N);
	for (int k = 0; k < N; k++)
	{
		out[k] = 0;
		for (int i = 0; i < N; i++)
		{
			out[k] += pow(wn, (double)(-k * i)) * val[i];
		}
	}*/

	const complex<double> I(0, 1);
	complex<double> temp(0, 0);
	for (int k = 0; k < N; k++)
	{
		for (int i = 0; i < N; i++)
		{
			temp = { (-2. * ((double)k) * ((double)i)) / ((double)N), 0 };
			out[k] += val[i] * exp(I * M_PI * temp) / ((double)N);
		}
	}
}


/*
void idft(int N, complex<double> *val, double *out)
{
	const complex<double> wn = wN((double)N);
	for (int n = 0; n < N; n++)
	{
		out[n] = 0;
		for (int k = 0; k < N; k++)
		{
			out[n] += (val[k] * pow(wn, k * n)).real();
		}

		out[n] /= ((double)N);
	}
}
*/
void widmo_amplitudowe(int N, complex<double> *val, double *out)
{
	for (int k = 0; k < N; k++)
	{
		out[k] = sqrt(pow(val[k].real(), 2) + pow(val[k].imag(), 2));
	}
}

void wartosc_amplitudy(int N, double *val, double *out)
{
	for (int i = 0; i < N; i++)
	{
		out[i] = 10 * log10(val[i]);
	}
}

const int fs = 10;
const int N = 913 * fs;

double ton_funkcja(double x, double *v)
{
	int index = (int) round(x * fs);
	return v[index % (N + 1)];
}

void wykres(std::ofstream &stream, std::function<double(double)> func, double start, double end, double dt)
{
	stream << fixed;
	stream.precision(5);
	for (double t = start; t <= end; t += dt)
	{
		stream << t << "," << func(t) << '\n';
	}
}

void wykres(std::string f, std::function<double(double)> func, double start, double end, double dt)
{
	std::ofstream file(f);
	wykres(file, func, start, end, dt);
	file.close();
}

void wykres2(std::string f, std::function<double(double)> func, std::function<double(int)> funcX, int N, double start, double dt)
{
	std::ofstream file(f);
	file << fixed;
	file.precision(5);
	double t = start;
	for (int i = 0; i < N; i++)
	{
		file << funcX(i) << "," << func(t) << '\n';
		t += dt;
	}
	file.close();
}

double widmo_skala(int k, double fs, int N)
{
	return ((double)k) * (fs / ((double)N));
}

double funkcja_X(double t)
{
	return A * t * t + B * t + C;
}

double funkcja_Y(double t)
{
	return 2 * pow(funkcja_X(t), 2) + 12.0 * cos(t);
}

double funkcja_Z(double t)
{
	return sin(2 * M_PI * 7 * t) * funkcja_X(t) - 0.2 * log10(abs(funkcja_Y(t)) + M_PI);
}

double funkcja_U(double t)
{
	return sqrt(abs(funkcja_Y(t) * funkcja_Y(t) * funkcja_Z(t))) - 1.8 * sin(0.4 * t * funkcja_Z(t) * funkcja_X(t));
}

double funkcja_V(double t)
{
	if (t >= 0 && t < 0.22)
	{
		return (t - 7 * t) * sin((2 * M_PI * t * 10) / (t + 0.04));
	}
	else if (t >= 0.22 && t < 0.7)
	{
		return 0.63 * t * sin(125 * t);
	}

	return pow(t, -0.662) + 0.77 * sin(8 * t);
}

double funkcja_P(double t)
{
	double suma = 0;
	double N[] = { 2, 4, (A * 10 + B) };
	for (int i = 0; i < 3; i++)
	{
		suma += (cos(12 * t * pow(N[i], 2)) + cos(16 * t * N[i])) / pow(N[i], 2);
	}

	return suma;
}

double widmo_lab1_funkcja(double x, double *amplituda, double m)
{
	return amplituda[(int) round(x * m)];
}

double widmo_lab1_funkcjaX(double x, double *amplituda)
{
	double t = round((x + 10) * 100);
	return amplituda[(int) t];
}

void widmo_lab1(std::string f, std::function<double(double)> func)
{
	double *v = new double[22051];
	for (int i = 0; i <= 22050; i++)
	{
		v[i] = func(((double)i) / 22050.);
	}

	std::complex<double> *v_dft = new std::complex<double>[22051];
	dft(22051, v, v_dft);
	double *widmo = new double[22051];
	double *amplituda = new double[22051];
	widmo_amplitudowe(22051, v_dft, widmo);
	wartosc_amplitudy(22051, widmo, amplituda);
	std::function<double(double)> func_amplituda = std::bind(widmo_lab1_funkcja, std::placeholders::_1, amplituda, (double) 22050);
	std::function<double(int)> func_fk = std::bind(widmo_skala, std::placeholders::_1, 22050., 22050);
	wykres2(f, func_amplituda, func_fk, 22051, 0, 1. / 22050.);
	delete[] amplituda;
	delete[] widmo;
	delete[] v_dft;
	delete[] v;
}

void widmo_lab1_wykres1(std::string f)
{
	double *v = new double[20001];
	for (int i = 0; i <= 20000; i++)
	{
		double x = -10. + ((double)i) / 100.;
		v[i] = funkcja_X(x);
	}

	std::complex<double> *v_dft = new std::complex<double>[20001];
	dft(20001, v, v_dft);
	double *widmo = new double[20001];
	double *amplituda = new double[20001];
	widmo_amplitudowe(20001, v_dft, widmo);
	wartosc_amplitudy(20001, widmo, amplituda);
	std::function<double(int)> func_fk = std::bind(widmo_skala, std::placeholders::_1, 20000., 20000);
	std::function<double(double)> func_amplituda = std::bind(widmo_lab1_funkcjaX, std::placeholders::_1, amplituda);
	wykres2(f, func_amplituda, func_fk, 20001, -10, 1. / 100.);
	delete[] amplituda;
	delete[] widmo;
	delete[] v_dft;
	delete[] v;
}

int main()
{
	double *sygnal = new double[N + 1];
	std::function<double(double)> func = std::bind(ton_funkcja, std::placeholders::_1, sygnal);
	ton_prosty(N + 1, 1, B, C + M_PI, sygnal, 1. / ((double)fs));
	std::ofstream f("wykresy/w1.csv");
	wykres(f, func, 0, 913, 1. / ((double)fs));
	f.close();
	complex<double> *sygnalDFT = new complex<double>[N + 1];
	cout << "Wykres 2" << endl;
	dft(N + 1, sygnal, sygnalDFT);
	double *widmo = new double[N + 1];
	double *amplituda = new double[N + 1];
	widmo_amplitudowe(N + 1, sygnalDFT, widmo);
	wartosc_amplitudy(N + 1, widmo, amplituda);
	std::function<double(int)> func_fk = std::bind(widmo_skala, std::placeholders::_1, (double)fs, 9130);
	std::function<double(double)> func_amplituda = std::bind(ton_funkcja, std::placeholders::_1, amplituda);
	wykres2("wykresy/w2.csv", func_amplituda, func_fk, 9130, 0, 1. / ((double)fs));
	
	cout << "Wykres 3" << endl;
	widmo_lab1_wykres1("wykresy/w3.csv");
	cout << "Wykres 4" << endl;
	widmo_lab1("wykresy/w4.csv", funkcja_Y);
	cout << "Wykres 5" << endl;
	widmo_lab1("wykresy/w5.csv", funkcja_Z);
	cout << "Wykres 6" << endl;
	widmo_lab1("wykresy/w6.csv", funkcja_U);
	cout << "Wykres 7" << endl;
	widmo_lab1("wykresy/w7.csv", funkcja_V);
	cout << "Wykres 8" << endl;
	widmo_lab1("wykresy/w8.csv", funkcja_P);
	return 0;
}