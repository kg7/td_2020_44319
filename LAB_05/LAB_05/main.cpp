#define _USE_MATH_DEFINES

#include <iostream>
#include <cmath>
#include <functional>
#include <fstream>
#include <cstdint>
#include <complex>

#define A 9
#define B 1
#define C 3
#define D 4
#define E 4
#define F 1

using namespace std;

// czas trwania 1 bitu (ilo�� pr�bek)
constexpr int Tb = 10;

const double A1 = 0.5;
const double A2 = 1;

typedef uint8_t byte;

complex<double> wN(const double N)
{
	const double x = (2. * M_PI) / N;
	return complex<double>(cos(x), sin(x));
}



void dft(int N, double* val, complex<double>* out)
{
	/*const complex<double> wn = wN((double)N);
	for (int k = 0; k < N; k++)
	{
		out[k] = 0;
		for (int i = 0; i < N; i++)
		{
			out[k] += pow(wn, (double)(-k * i)) * val[i];
		}
	}*/

	const complex<double> I(0, 1);
	complex<double> temp(0, 0);
	for (int k = 0; k < N; k++)
	{
		for (int i = 0; i < N; i++)
		{
			temp = { (-2. * ((double)k) * ((double)i)) / ((double)N), 0 };
			out[k] += val[i] * exp(I * M_PI * temp) / ((double)N);
		}
	}
}


/*
void idft(int N, complex<double> *val, double *out)
{
	const complex<double> wn = wN((double)N);
	for (int n = 0; n < N; n++)
	{
		out[n] = 0;
		for (int k = 0; k < N; k++)
		{
			out[n] += (val[k] * pow(wn, k * n)).real();
		}

		out[n] /= ((double)N);
	}
}
*/
void widmo_amplitudowe(int N, complex<double>* val, double* out)
{
	for (int k = 0; k < N; k++)
	{
		out[k] = sqrt(pow(val[k].real(), 2) + pow(val[k].imag(), 2));
	}
}

void wartosc_amplitudy(int N, double* val, double* out)
{
	for (int i = 0; i < N; i++)
	{
		out[i] = 10 * log10(val[i]);
	}
}

// rozmiar out: N * 8 * Tb
double *kluczowanie_amplitudy(byte* in, int N, double przes, double dt)
{
	double* out = new double[N * 8 * Tb];
	const double f = ((double)N) / ((double)Tb);
	double t = 0;
	int index = 0;
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			byte bit = (in[i] >> j) & 0x1;
			if (bit == 0)
			{
				out[index] = A1 * sin(2 * M_PI * f * t + przes);
			}
			else
			{
				out[index] = A2 * sin(2 * M_PI * f * t + przes);
			}

			index++;
			t += dt;
		}
	}

	return out;
}

double *kluczowanie_czestotliwosci(byte* in, int N, double przes, double dt)
{
	double* out = new double[N * 8 * Tb];
	const double f0 = ((double)(N + 1)) / ((double)Tb);
	const double f1 = ((double)(N + 2)) / ((double)Tb);
	const double a = A2;
	double t = 0;
	int index = 0;
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			byte bit = (in[i] >> j) & 0x1;
			if (bit == 0)
			{
				out[index] = a * sin(2 * M_PI * f0 * t + przes);
			}
			else
			{
				out[index] = a * sin(2 * M_PI * f1 * t + przes);
			}

			index++;
			t += dt;
		}
	}

	return out;
}

double *kluczowanie_fazy(byte* in, int N, double dt)
{
	double* out = new double[N * 8 * Tb];
	const double a = A2;
	const double f = ((double)N) / ((double)Tb);
	double t = 0;
	int index = 0;
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			byte bit = (in[i] >> j) & 0x1;
			if (bit == 0)
			{
				out[index] = a * sin(2 * M_PI * f * t);
			}
			else
			{
				out[index] = a * sin(2 * M_PI * f * t + M_PI);
			}

			index++;
			t += dt;
		}
	}

	return out;
}

void S2BS(std::string& in, bool littleEndian, byte* out)
{
	const size_t size = in.size();
	if (littleEndian)
	{
		for (size_t i = 0; i < size; i++)
		{
			out[i] = in[size - i - 1];
		}
	}
	else
	{
		for (size_t i = 0; i < size; i++)
		{
			out[i] = in[i];
		}
	}
}

byte* S2BS(std::string& in, bool littleEndian)
{
	byte* out = new byte[in.size()];
	S2BS(in, littleEndian, out);
	return out;
}

byte* S2BS(std::string&& in, bool littleEndian)
{
	return S2BS(in, littleEndian);
}

void printHex(byte* in, size_t len)
{
	cout << hex << uppercase;
	for (size_t i = 0; i < len; i++)
	{
		cout << (int)in[i] << " ";
	}

	cout << dec << nouppercase << endl;
}

// rozmiar: N * 8 * tb
double* wygenerujSygnalInformacyjny(byte* in, int N, int tb)
{
	double *out = new double[N * 8 * tb];
	int index = 0;
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			byte bit = (in[i] >> j) & 0x01;
			for (int k = 0; k < tb; k++)
			{
				out[index++] = bit;
			}
		}
	}

	return out;
}

void wykres(std::ofstream& stream, std::function<double(double)> func, double start, double end, double dt)
{
	stream << fixed;
	stream.precision(5);
	for (double t = start; t <= end; t += dt)
	{
		stream << t << "," << func(t) << '\n';
	}
}

void wykres(std::string f, std::function<double(double)> func, double start, double end, double dt)
{
	std::ofstream file(f);
	wykres(file, func, start, end, dt);
	file.close();
}

double wykres_funkcja(double t, double *data)
{
	return data[(size_t)t];
}

void wykres(std::string f, double* data, int N)
{
	std::function<double(double)> func = std::bind(wykres_funkcja, std::placeholders::_1, data);
	wykres(f, func, 0, N, 1);
}


double widmo_skala(int k, double fs, int N)
{
	return ((double)k) * (fs / ((double)N));
}

double ton_funkcja(double x, double* v, int N)
{
	int index = (int)round(x);
	return v[index % (N)];
}

void wykres2(std::string f, std::function<double(double)> func, std::function<double(int)> funcX, int N, double start, double dt)
{
	std::ofstream file(f);
	file << fixed;
	file.precision(5);
	double t = start;
	for (int i = 0; i < N; i++)
	{
		file << funcX(i) << "," << func(t) << '\n';
		t += dt;
	}
	file.close();
}

void widmo(std::string f, double* sygnal, int N)
{
	complex<double> *syg_dft = new complex<double>[N];
	dft(N, sygnal, syg_dft);
	double* widmo = new double[N];
	double* amplituda = new double[N];
	widmo_amplitudowe(N, syg_dft, widmo);
	wartosc_amplitudy(N, widmo, amplituda);
	std::function<double(int)> func_fk = std::bind(widmo_skala, std::placeholders::_1, (double)1, N);
	std::function<double(double)> func_amplituda = std::bind(ton_funkcja, std::placeholders::_1, amplituda, N);
	wykres2(f, func_amplituda, func_fk, N, 0, 1);
	delete[] syg_dft;
	delete[] widmo;
	delete[] amplituda;
}

// Przyk�ad S2BS (w formie szesnastkowej): Ala ma kota
// Big endian:		41 6C 61 20 6D 61 20 6B 6F 74 61
// Little endian:	61 74 6F 6B 20 61 6D 20 61 6C 41

int main()
{
	/*

	// Przyk�ad S2BS

	std::string przyklad = "Ala ma kota";
	byte *strBE = S2BS(przyklad, false);
	byte *strLE = S2BS(przyklad, true);

	printHex(strBE, przyklad.size());
	printHex(strLE, przyklad.size());

	delete[] strBE;
	delete[] strLE;
	*/
	string dane = "Krystian Gorecki - Lab 5";
	byte* strumien = S2BS(dane, false);
	double* sygnal_informacyjny = wygenerujSygnalInformacyjny(strumien, dane.length(), Tb);
	wykres("wykresy/syg_informacyjny.csv", sygnal_informacyjny, 10 * Tb);
	widmo("wykresy/syg_informacyjny_widmo.csv", sygnal_informacyjny, dane.length() * 8 * Tb);
	delete[] sygnal_informacyjny;

	double* sygnal_ASK = kluczowanie_amplitudy(strumien, dane.length(), 0, 0.1);
	wykres("wykresy/syg_ASK.csv", sygnal_ASK, 10 * Tb);
	widmo("wykresy/syg_ASK_widmo.csv", sygnal_ASK, dane.length() * 8 * Tb);
	delete[] sygnal_ASK;

	double* sygnal_FSK = kluczowanie_czestotliwosci(strumien, dane.length(), 0, 0.1);
	wykres("wykresy/syg_FSK.csv", sygnal_FSK, 10 * Tb);
	widmo("wykresy/syg_FSK_widmo.csv", sygnal_FSK, dane.length() * 8 * Tb);
	delete[] sygnal_FSK;

	double* sygnal_PSK = kluczowanie_fazy(strumien, dane.length(), 0.1);
	wykres("wykresy/syg_PSK.csv", sygnal_PSK, 10 * Tb);
	widmo("wykresy/syg_PSK_widmo.csv", sygnal_PSK, dane.length() * 8 * Tb);
	delete[] sygnal_PSK;
	delete[] strumien;




	return 0;
}