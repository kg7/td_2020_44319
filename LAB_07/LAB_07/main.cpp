#include <iostream>
#include <string>
#include <functional>
#include <fstream>

using namespace std;

const int Tb = 30;

#define SIZE(f) ((int)(round(f * 2) * Tb))

typedef uint8_t byte;

double* sygnalZegarowy(double f)
{
	int N = SIZE(f);
	double* sygnal = new double[N];
	uint8_t bit = 1;
	for (int i = 0; i < N; i++)
	{
		if (i % Tb == 0)
		{
			if (bit == 0)
			{
				bit = 1;
			}
			else
			{
				bit = 0;
			}
		}

		sygnal[i] = bit;
	}

	return sygnal;
}

void wykres(std::ofstream& stream, std::function<double(double)> func, double start, double end, double dt)
{
	stream << fixed;
	stream.precision(5);
	for (double t = start; t <= end; t += dt)
	{
		stream << t << "," << func(t) << '\n';
	}
}

void wykres(std::string f, std::function<double(double)> func, double start, double end, double dt)
{
	std::ofstream file(f);
	wykres(file, func, start, end, dt);
	file.close();
}

void wykres2(std::string f, std::function<double(int)> func, std::function<double(int)> funcX, int N)
{
	std::ofstream file(f);
	file << fixed;
	file.precision(5);
	for (int i = 0; i < N; i++)
	{
		file << funcX(i) << "," << func(i) << '\n';
	}
	file.close();
}

double f_X(int index, int N)
{
	return ((double) index) / ((double)N);
}

double f_Y(int index, double* data)
{
	return data[index];
}

void S2BS(std::string& in, bool littleEndian, byte* out)
{
	const size_t size = in.size();
	if (littleEndian)
	{
		for (size_t i = 0; i < size; i++)
		{
			out[i] = in[size - i - 1];
		}
	}
	else
	{
		for (size_t i = 0; i < size; i++)
		{
			out[i] = in[i];
		}
	}
}

byte* S2BS(std::string& in, bool littleEndian)
{
	byte* out = new byte[in.size()];
	S2BS(in, littleEndian, out);
	return out;
}

byte* S2BS(std::string&& in, bool littleEndian)
{
	return S2BS(in, littleEndian);
}

// rozmiar: N * 8 * tb
double* wygenerujSygnalInformacyjny(byte* in, int N, int tb)
{
	double* out = new double[N * 8 * tb];
	int index = 0;
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			byte bit = (in[i] >> j) & 0x01;
			for (int k = 0; k < tb; k++)
			{
				out[index++] = bit;
			}
		}
	}

	return out;
}

double* manchester(byte* strumien, int N, int tb)
{
	double* out = new double[N * 8 * tb];
	int tb2 = tb / 2;
	int index = 0;

	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			byte bit = (strumien[i] >> j) & 0x01;
			if (bit == 0)
			{
				for (int k = 0; k < tb2; k++)
				{
					out[index++] = 1;
				}

				for (int k = 0; k < tb2; k++)
				{
					out[index++] = 0;
				}
			}
			else
			{
				for (int k = 0; k < tb2; k++)
				{
					out[index++] = 0;
				}

				for (int k = 0; k < tb2; k++)
				{
					out[index++] = 1;
				}
			}
		}
	}

	return out;
}

double* NRZI(byte* strumien, int N, int tb)
{
	double* out = new double[N * 8 * tb];
	int index = 0;

	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			byte bit = (strumien[i] >> j) & 0x01;
			if (bit == 1)
			{
				byte tmp = 0;
				if (index > 0)
				{
					tmp = out[index - 1] != 0 ? 0 : 1;
				}

				for (int k = 0; k < tb; k++)
				{
					out[index++] = tmp;
				}
			}
			else
			{
				for (int k = 0; k < tb; k++)
				{
					if (index == 0)
					{
						out[index++] = 0;
						continue;
					}

					out[index] = out[index - 1];
					index++;
				}
			}
		}
	}

	return out;
}


double* BAMI(byte* strumien, int N, int tb)
{
	double* out = new double[N * 8 * tb];
	int index = 0;
	
	int lastP = 1;

	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			byte bit = (strumien[i] >> j) & 0x01;
			if (bit == 1)
			{
				lastP = -1 * lastP;
				for (int k = 0; k < tb; k++)
				{
					out[index++] = lastP;
				}
			}
			else
			{
				for (int k = 0; k < tb; k++)
				{
					out[index++] = 0.5;
				}
			}
		}
	}

	return out;
}


int main()
{
	double f = 16;
	double* syg = sygnalZegarowy(f);
	int N = SIZE(f);
	std::function<double(int)> func_X = std::bind(f_X, std::placeholders::_1, N);
	std::function<double(int)> func_Y = std::bind(f_Y, std::placeholders::_1, syg);
	wykres2("wykresy/zegar.csv", func_Y, func_X, N);

	byte* strumien = S2BS("GK", false);
	double* sygnal_inf = wygenerujSygnalInformacyjny(strumien, 2, Tb * 2);
	func_Y = std::bind(f_Y, std::placeholders::_1, sygnal_inf);
	func_X = std::bind(f_X, std::placeholders::_1, 16 * Tb * 2);
	wykres2("wykresy/TTL.csv", func_Y, func_X, 16 * Tb * 2);
	delete[] sygnal_inf;
	
	double* syg_manchester = manchester(strumien, 2, Tb * 2);
	func_Y = std::bind(f_Y, std::placeholders::_1, syg_manchester);
	func_X = std::bind(f_X, std::placeholders::_1, 16 * Tb * 2);
	wykres2("wykresy/manchester.csv", func_Y, func_X, 16 * Tb * 2);
	delete[] syg_manchester;

	double* syg_NRZI = NRZI(strumien, 2, Tb * 2);
	func_Y = std::bind(f_Y, std::placeholders::_1, syg_NRZI);
	func_X = std::bind(f_X, std::placeholders::_1, 16 * Tb * 2);
	wykres2("wykresy/NRZI.csv", func_Y, func_X, 16 * Tb * 2);
	delete[] syg_NRZI;

	double* syg_BAMI = BAMI(strumien, 2, Tb * 2);
	func_Y = std::bind(f_Y, std::placeholders::_1, syg_BAMI);
	func_X = std::bind(f_X, std::placeholders::_1, 16 * Tb * 2);
	wykres2("wykresy/BAMI.csv", func_Y, func_X, 16 * Tb * 2);
	delete[] syg_BAMI;


	delete[] syg;
	return 0;
}