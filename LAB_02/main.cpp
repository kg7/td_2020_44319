#define _USE_MATH_DEFINES

#include <iostream>
#include <cmath>
#include <functional>
#include <fstream>

#define A 9
#define B 1
#define C 3
#define D 4
#define E 4
#define F 1

using namespace std;


void ton_prosty(double N, double a, double f, double przes, double *out, double dt)
{
	double t = 0;
	for (int i = 0; i < N; i++)
	{
		out[i] = a * sin((2. * M_PI * f * ((double)t)) + przes);
		t += dt;
	}
}

void wykres(std::ofstream &stream, std::function<double(double)> func, double start, double end, double dt)
{
	stream << fixed;
	stream.precision(5);
	for (double t = start; t <= end; t += dt)
	{
		stream << t << "," << func(t) << '\n';
	}
}

double ton_funkcja(double x, double *v)
{
	int index = (int)(x * 100);
	return v[index % 901];
}

double ton_funkcja2(double x, double *v)
{
	int index = (int)(x * 50);
	return v[index % 451];
}

void wykres(std::string f, std::function<double(double)> func, double start, double end, double dt)
{
	std::ofstream file(f);
	wykres(file, func, start, end, dt);
	file.close();
}

void kwantyzacja(double *in, int N, double a, int q, double *out)
{
	const double max = pow(2, q);

	for (int i = 0; i < N; i++)
	{
		out[i] = floor((in[i] / a) * max);
	}
}

int main()
{
	double *sygnal = new double[901]; // zakres <0, A>; A = 9
	constexpr double dt = A / 900.;
	ton_prosty(901, 1, B, C * M_PI, sygnal, dt);
	std::function<double(double)> func = std::bind(ton_funkcja, placeholders::_1, sygnal);
	wykres("wykresy/wykres1.csv", func, 0, A, 1. / 100.);
	double *sygnalKwantyzacja = new double[901];
	kwantyzacja(sygnal, 901, 1, 16, sygnalKwantyzacja);
	std::function<double(double)> func2 = std::bind(ton_funkcja, placeholders::_1, sygnalKwantyzacja);
	wykres("wykresy/wykres2.csv", func2, 0, A, 1. / 100.);
	ton_prosty(451, 1, B, C * M_PI, sygnal, dt * 2);
	kwantyzacja(sygnal, 451, 1, 8, sygnalKwantyzacja);
	std::function<double(double)> func3 = std::bind(ton_funkcja2, placeholders::_1, sygnalKwantyzacja);
	wykres("wykresy/wykres3.csv", func3, 0, A, 1. / 50.);
	delete[] sygnal;
	delete[] sygnalKwantyzacja;
	return 0;
}