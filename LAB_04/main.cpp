#define _USE_MATH_DEFINES

#include <iostream>
#include <complex>
#include <cmath>
#include <functional>
#include <fstream>

#define A 9
#define B 1
#define C 3
#define D 4
#define E 4
#define F 1

using namespace std;


void ton_prosty(double N, double a, double f, double przes, double *out, double dt)
{
	double t = 0;
	for (int i = 0; i < N; i++)
	{
		out[i] = a * sin((2. * M_PI * f * ((double)t)) + przes);
		t += dt;
	}
}

// kA - amplituda sygnalu nosnego
// fn - czestotliwosc sygnalu nosnego

void modulacja_amplitudy(double N, double kA, double fn, double *in, double *out, double dt)
{
	double t = 0;
	for (int i = 0; i < N; i++)
	{
		out[i] = (kA * in[i] + 1) * cos(2 * M_PI * fn * t);
		t += dt;
	}
}

// fn - czestotliwosc sygnalu nosnego
void modulacja_fazy(double N, double kP, double fn, double *in, double *out, double dt)
{
	double t = 0;
	for (int i = 0; i < N; i++)
	{
		out[i] = cos(2 * M_PI * fn * t + kP * in[i]);
		t += dt;
	}
}

void wykres(std::ofstream &stream, std::function<double(double)> func, double start, double end, double dt)
{
	stream << fixed;
	stream.precision(5);
	for (double t = start; t <= end; t += dt)
	{
		stream << t << "," << func(t) << '\n';
	}
}

double ton_funkcja(double x, double *v)
{
	int index = (int)(x * 913);
	return v[index % 914];
}

double widmo_funkcja(double x, double *v)
{
	return v[((int)round(x)) % 914];
}

void wykres(std::string f, std::function<double(double)> func, double start, double end, double dt)
{
	std::ofstream file(f);
	wykres(file, func, start, end, dt);
	file.close();
}

void dft(int N, double *val, complex<double> *out)
{
	const complex<double> I(0, 1);
	complex<double> temp(0, 0);
	for (int k = 0; k < N; k++)
	{
		for (int i = 0; i < N; i++)
		{
			temp = { (-2. * ((double)k) * ((double)i)) / ((double)N), 0 };
			out[k] += val[i] * exp(I * M_PI * temp) / ((double)N);
		}
	}
}

void widmo_amplitudowe(int N, complex<double> *val, double *out)
{
	for (int k = 0; k < N; k++)
	{
		out[k] = sqrt(pow(val[k].real(), 2) + pow(val[k].imag(), 2));
	}
}

void wartosc_amplitudy(int N, double *val, double *out)
{
	for (int i = 0; i < N; i++)
	{
		out[i] = 10 * log10(val[i]);
	}
}

void widmo(std::string f, double *syg)
{
	int N = 913;
	std::complex<double> *sygnalDFT = new std::complex<double>[N + 1];
	dft(N + 1, syg, sygnalDFT);
	double *widmo = new double[N + 1];
	double *amplituda = new double[N + 1];
	widmo_amplitudowe(N + 1, sygnalDFT, widmo);
	wartosc_amplitudy(N + 1, widmo, amplituda);
	std::function<double(double)> func = std::bind(widmo_funkcja, std::placeholders::_1, amplituda);
	wykres(f, func, 0, N, 1);
	delete[] amplituda;
	delete[] widmo;
	delete[] sygnalDFT;
}


int main()
{
	int N = 913;
	const int fn = 5;
	double *sygnal = new double[N + 1];
	ton_prosty(N + 1, 1, B, C * M_PI, sygnal, 1. / N);
	std::function<double(double)> func1 = std::bind(ton_funkcja, std::placeholders::_1, sygnal);
	wykres("wykresy/syg_informacyjny.csv", func1, 0, 5, 1. / N);

	double *AM_sygnal_a = new double[N + 1];
	modulacja_amplitudy(N + 1, 0.5, fn, sygnal, AM_sygnal_a, 1. / N);
	std::function<double(double)> funcAM = std::bind(ton_funkcja, std::placeholders::_1, AM_sygnal_a);
	wykres("wykresy/syg_AM_a.csv", funcAM, 0, 5, 1. / N);

	double *PM_sygnal_a = new double[N + 1];
	modulacja_fazy(N + 1, 1.5, fn, sygnal, PM_sygnal_a, 1. / N);
	std::function<double(double)> funcPM = std::bind(ton_funkcja, std::placeholders::_1, PM_sygnal_a);
	wykres("wykresy/syg_PM_a.csv", funcPM, 0, 5, 1. / N);

	double *AM_sygnal_b = new double[N + 1];
	modulacja_amplitudy(N + 1, 5, fn, sygnal, AM_sygnal_b, 1. / N);
	funcAM = std::bind(ton_funkcja, std::placeholders::_1, AM_sygnal_b);
	wykres("wykresy/syg_AM_b.csv", funcAM, 0, 5, 1. / N);

	double *PM_sygnal_b = new double[N + 1];
	modulacja_fazy(N + 1, 2.8, fn, sygnal, PM_sygnal_b, 1. / N);
	funcPM = std::bind(ton_funkcja, std::placeholders::_1, PM_sygnal_b);
	wykres("wykresy/syg_PM_b.csv", funcPM, 0, 5, 1. / N);

	double *AM_sygnal_c = new double[N + 1];
	modulacja_amplitudy(N + 1, 25, fn, sygnal, AM_sygnal_c, 1. / N);
	funcAM = std::bind(ton_funkcja, std::placeholders::_1, AM_sygnal_c);
	wykres("wykresy/syg_AM_c.csv", funcAM, 0, 5, 1. / N);

	double *PM_sygnal_c = new double[N + 1];
	modulacja_fazy(N + 1, 23, fn, sygnal, PM_sygnal_c, 1. / N);
	funcPM = std::bind(ton_funkcja, std::placeholders::_1, PM_sygnal_c);
	wykres("wykresy/syg_PM_c.csv", funcPM, 0, 5, 1. / N);

	widmo("wykresy/widmo_AM_a.csv", AM_sygnal_a);
	widmo("wykresy/widmo_AM_b.csv", AM_sygnal_b);
	widmo("wykresy/widmo_AM_c.csv", AM_sygnal_c);

	widmo("wykresy/widmo_PM_a.csv", PM_sygnal_a);
	widmo("wykresy/widmo_PM_b.csv", PM_sygnal_b);
	widmo("wykresy/widmo_PM_c.csv", PM_sygnal_c);

	delete[] PM_sygnal_c;
	delete[] AM_sygnal_c;
	delete[] PM_sygnal_b;
	delete[] AM_sygnal_b;
	delete[] PM_sygnal_a;
	delete[] AM_sygnal_a;
	delete[] sygnal;
	return 0;
}