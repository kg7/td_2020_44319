#include <iostream>
#include <fstream>
#include <cmath>
#include <functional>

#define A 9
#define B 1
#define C 3
#define D 4
#define E 4
#define F 1

#define PI 3.141592653589793238462643383279502884197169

using namespace std;


int miejsce_zerowe(double a, double b, double c, double &t1, double &t2, double &delta)
{
	delta = (double)b * b - 4 * a * c;
	if (delta < 0)
	{
		return 0;
	}

	delta = sqrt(delta);

	t1 = (-b + delta) / 2 * a;
	t2 = (-b - delta) / 2 * a;

	if (delta == 0)
	{
		return 1;
	}

	return 2;
}

double funkcja_X(double t)
{
	return A * t * t + B * t + C;
}

double funkcja_Y(double t)
{
	return 2 * pow(funkcja_X(t), 2) + 12.0 * cos(t);
}

double funkcja_Z(double t)
{
	return sin(2 * PI * 7 * t) * funkcja_X(t) - 0.2 * log10(abs(funkcja_Y(t)) + PI);
}

double funkcja_U(double t)
{
	return sqrt(abs(funkcja_Y(t) * funkcja_Y(t) * funkcja_Z(t))) - 1.8 * sin(0.4 * t * funkcja_Z(t) * funkcja_X(t));
}

double funkcja_V(double t)
{
	if (t >= 0 && t < 0.22)
	{
		return (t - 7 * t) * sin((2 * PI * t * 10) / (t + 0.04));
	}
	else if (t >= 0.22 && t < 0.7)
	{
		return 0.63 * t * sin(125 * t);
	}

	return pow(t, -0.662) + 0.77 * sin(8 * t);
}

double funkcja_P(double t)
{
	double suma = 0;
	double N[] = { 2, 4, (A * 10 + B) };
	for (int i = 0; i < 3; i++)
	{
		suma += (cos(12 * t * pow(N[i], 2)) + cos(16 * t * N[i])) / pow(N[i], 2);
	}

	return suma;
}

void wykres(std::ofstream &stream, std::function<double(double)> func, double start, double end, double dt)
{
	stream << fixed;
	stream.precision(5);

	for (double t = start; t <= end; t += dt)
	{
		stream << t << "," << func(t) << '\n';
	}
}


int main()
{
	// Zadanie 1

	// x(t) = 9 * t^2 + 1 * t + 3
	double t1, t2, delta;
	int miejsca_zerowe = miejsce_zerowe(A, B, C, t1, t2, delta);
	cout << "delta: " << delta << endl;
	if (miejsca_zerowe == 0)
	{
		cout << "Brak miejsc zerowych" << endl;
	}
	else if (miejsca_zerowe == 1)
	{
		cout << "t = " << t1 << endl;
	}
	else
	{
		cout << "t1 = " << t1 << endl;
		cout << "t2 = " << t2 << endl;
	}

	std::ofstream x("wykresy/wykresX.csv");
	wykres(x, funkcja_X, -10, 10, 1. / 100.);
	x.close();

	std::ofstream y("wykresy/wykresY.csv");
	wykres(y, funkcja_Y, 0, 1, 1. / 22050.);
	y.close();

	std::ofstream z("wykresy/wykresZ.csv");
	wykres(z, funkcja_Z, 0, 1, 1. / 22050.);
	z.close();

	std::ofstream u("wykresy/wykresU.csv");
	wykres(u, funkcja_U, 0, 1, 1. / 22050.);
	u.close();

	std::ofstream v("wykresy/wykresV.csv");
	wykres(v, funkcja_V, 0, 1, 1. / 22050.);
	v.close();

	std::ofstream p("wykresy/wykresP.csv");
	wykres(p, funkcja_P, 0, 1, 1. / 22050.);
	p.close();
	return 0;
}